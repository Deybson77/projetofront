import { Component, OnInit, Input } from '@angular/core';
import {FormGroup } from '@angular/forms'

import { ApiService } from '../api.service';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {


  avenida: {};
  constructor(private api : ApiService) { }

  ngOnInit() {
    this.avenida = {}
  }
  cadastrarAvenida(form){ 
    this.api.salvarAvenida(this.avenida).subscribe(res =>{
      alert("Cadastrado com sucesso")
      this.avenida = {}
      
    })
   
  }
}