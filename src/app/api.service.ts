import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  urlApi : string = "http://localhost:3000";
  constructor(private http: HttpClient) { }

  listarAvenidas(){
    return this.http.get<any>(`${this.urlApi}/avenidas`);
  }

  excluirAvenida(id: number) {
    return this.http.delete<any>(`${this.urlApi}/avenidas/${id}`)
  }

  salvarAvenida(body: any) {
    return this.http.post<any>(`${this.urlApi}/avenidas`, body)
  }

  buscar(nome: string){
    return this.http.get<any>(`${this.urlApi}/avenidas/${nome}`)

  }

}
