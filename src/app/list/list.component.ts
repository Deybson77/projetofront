import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  avenidas = [];
  nomeFiltro: string
  avenida: {}
 
  constructor(private api: ApiService) { }

  ngOnInit() { 
    this.nomeFiltro = ""
    this.api.listarAvenidas().subscribe(resp => {
      resp.avenida.forEach(avenida => {
        avenida.ciclofaixa = avenida.ciclofaixa == 0 ? "Não" : "sim"
        this.avenidas.push(avenida)
      })
      sessionStorage.setItem('avenidas', JSON.stringify(this.avenidas)) 
    })
    
  }

  excluirAvenida(id : number, index: number){
    this.api.excluirAvenida(id).subscribe(resp => {
      alert(resp.msg)
      this.avenidas.splice(index, 1)
    })
  }

  filtrar(nome: string){
    this.api.buscar(this.nomeFiltro).subscribe(res =>{
      if(res.avenida.length === 0 ){ 
        alert('Resultado da busca não existe')
      }else{
        this.avenida = res.avenida[0]
        console.log(this.avenida)
      }
    })
  }

}
